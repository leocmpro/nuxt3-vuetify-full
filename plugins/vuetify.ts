import "@fortawesome/fontawesome-free/css/all.css";
import colors from "vuetify/util/colors";

import "@/assets/main.scss";
import { createVuetify } from "vuetify";
import { aliases, fa } from "vuetify/iconsets/fa";
import { en, es } from "vuetify/locale";

export default defineNuxtPlugin((app) => {
  const vuetify = createVuetify({
    ssr: true,
    // https://vuetifyjs.com/en/features/global-configuration/#global-configuration
    defaults: {
      VBtn: {
        rounded: "lg",
      },
    },
    // https://vuetifyjs.com/en/features/internationalization/#internationalization-i18n
    locale: {
      locale: "es",
      fallback: "es",
      messages: { en, es },
    },
    // https://vuetifyjs.com/en/features/icon-fonts/#fa-5-css
    icons: {
      defaultSet: "fa",
      aliases,
      sets: {
        fa,
      },
    },
    // https://vuetifyjs.com/en/features/theme/#setup
    theme: {
      defaultTheme: "light",
      themes: {
        light: {
          dark: false,
          colors: {
            primary: colors.red.darken1, // #E53935
            secondary: colors.red.lighten4, // #FFCDD2
            // ...
          },
        },
      },
    },
  });
  app.vueApp.use(vuetify);
});
